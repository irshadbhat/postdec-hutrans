===============
postdec-hutrans
===============

Hindi-to-Urdu and Urdu-to-Hidi transliterator with context based transliteration.

References
==========

`Dependency Parsing of Hindi and Urdu (Under Review)`_

.. _`Dependency Parsing of Hindi and Urdu (Under Review)`: https://researchweb.iiit.ac.in/~riyaz.bhat/

Installation
============

Dependencies
~~~~~~~~~~~~

`postdec-hutrans`_ requires `cython`_, `SciPy`_, `KenLM`_ and `indic-wx-converter`_.

.. _`postdec-hutrans`: https://bitbucket.org/irshadbhat/postdec-hutrans

.. _`cython`: http://docs.cython.org/src/quickstart/install.html

.. _`Scipy`: http://www.scipy.org/install.html

.. _`KenLM`: https://github.com/kpu/kenlm

.. _`indic-wx-converter`: https://github.com/irshadbhat/indic-wx-converter

To install the dependencies do something like (Ubuntu):

::

    pip install cython
    pip install scipy
    pip install https://github.com/kpu/kenlm/archive/master.zip
    pip install git+git://github.com/irshadbhat/indic-wx-converter.git

Download
~~~~~~~~

Download **postdec-hutrans**  from `bitbucket`_.

.. _`bitbucket`: https://bitbucket.org/irshadbhat/postdec-hutrans

Install
~~~~~~~

::

    cd postdec-hutrans
    sudo python setup.py install

    OR

    pip install git+git://bitbucket.org/irshadbhat/postdec-hutrans.git

Examples
~~~~~~~~

1. **Work with Files:**

::

    pdtrans --i tests/urdu.txt --s urdu --o /tmp/urdu-dev.txt
    pdtrans --i tests/hindi.txt --s hindi --o /tmp/hindi-parab.txt

    --i input          <input-file>
    --o output         <output-file>
    --s source         source script [hindi|urdu]
    --b beam_width     beam-width
    --no-tokenization  set this flag to keep off input tokenization

2. **From Python:**

::

    >>> from pdtrans import transliterator
    >>> trn = transliterator(source='urdu')
    >>> 
    >>> text = u'میں کلکتہ میں رہا کرتا تھا۔'
    >>> print trn.transform(text)
    >>> 
    में कलकत्ता में रहा करता था .
    >>>
    >>> text = u'کیا ہوا کہ ہوا کافی تیز چل رہی تھی۔'
    >>> print trn.transform(text)
    >>>
    क्या हुआ कि हवा काफी तेज चल रही थी .
    >>>
    >>> text = u'وہ بہت دور سے چلا رہا تھا۔'
    >>> print trn.transform(text)
    >>>
    वह बहुत दूर से चला रहा था .
    >>>
    >>> text = u'وہ بہت دور سے چللا رہا تھا۔'
    >>> print trn.transform(text)
    >>>
    वह बहुत दूर से चिल्ला रहा था .
    >>> 
