#! /usr/bin/env python
# -*- encoding: utf-8 -*-

import re
import sys
import argparse

from transliterator import transliterator

__doc__        = "Hindi to Urdu and Urdu to HIndi transliterator"
__author__     = ["Riyaz Ahmad", "Irshad Ahmad"]
__version__    = "1.0"
__copyright__  = "Copyright (C) 2015 Riyaz Ahmad, Irshad Ahmad"
__license__    = "MIT"
__maintainer__ = "Irshad Ahmad"
__email__      = "irshad.bhat@research.iiit.ac.in"

def main():
    # parse command line arguments 
    parser = argparse.ArgumentParser(prog="hutrans-lm", description="Hindi-to-Urdu and Urdu-to-Hindi Transliterator")
    parser.add_argument('--v', action="version", version="%(prog)s 1.0")
    parser.add_argument('--s', metavar='source', dest="source", choices=["hindi","urdu"], default="hindi", help="source script [hindi|urdu]")
    parser.add_argument('--i', metavar='input', dest="INFILE", type=argparse.FileType('r'), default=sys.stdin, help="<input-file>")
    parser.add_argument('--b', metavar='beam_width', dest="beam_width", type=int, choices=range(3, 21), default=3, help="beam-width")
    parser.add_argument('--no-tokenization', dest='no_tok', action='store_false', help="set this flag to keep off tokenization")
    parser.add_argument('--o', metavar='output', dest="OUTFILE", type=argparse.FileType('w'), default=sys.stdout, help="<output-file>")

    args = parser.parse_args()

    # initialize transliterator object
    trn = transliterator(args.source, args.beam_width, args.no_tok)

    # transliterate text
    for line in args.INFILE:
        trans_line = trn.convert(line)
	args.OUTFILE.write(trans_line)
    
    # close files 
    args.INFILE.close()
    args.OUTFILE.close()
