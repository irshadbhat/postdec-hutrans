#!/usr/bin/env python 
#!-*- coding: utf-8 -*-

#Copyright (C) 2015 Irshad Ahmad Bhat

"""Persio-Arabic to Devnagari transliteration"""

import re
import sys
import string
import os.path
import warnings

import kenlm
import numpy as np

from converter_indic import wxConvert
from _decode import beamsearch, so_viterbi

warnings.filterwarnings("ignore")

class PD_Transliterator():
    def __init__(self, beam_width=3, tokenize=True): 
        self.lookup = dict()
        self.space = '\x02\x03' 
        self.tokenize = tokenize
	self.beam_width = beam_width
	self.topn = min(self.beam_width - 1, 10)

	self.fit()

    def fit(self):
        self.con = wxConvert(order='wx2utf', rmask=True)
        dist_dir = os.path.dirname(os.path.abspath(__file__))

	#load models
	sys.path.append('%s/_utils' %dist_dir)
	self.coef_	      = np.load('%s/models/uh_coef.npy' %dist_dir)[0]
	self.classes_	      = np.load('%s/models/uh_classes.npy' %dist_dir)[0]
        self.vectorizer_      = np.load('%s/models/uh_sparse-vec.npy' %dist_dir)[0]
        self.intercept_init_  = np.load('%s/models/uh_intercept_init.npy' %dist_dir)
        self.intercept_trans_ = np.load('%s/models/uh_intercept_trans.npy' %dist_dir)
        self.intercept_final_ = np.load('%s/models/uh_intercept_final.npy' %dist_dir)
    
	#load language model
	blm =  kenlm.LanguageModel('%s/lm/hindi-n3.blm' %dist_dir)
	self.so_dec = so_viterbi(blm)

	#compile regexes
	alpha = u'\u0621-\u063a\u0641-\u064a\u0674-\u06d3'
	sos_chars = u'\u0617-\u061a\u0620-\u065f\u066e-\u06d3\u06d5\u06fa-\u06ff\u201d\u2019A-Z'
	self.mask_rom = re.compile(r'([a-zA-Z]+)')
	self.non_alpha = re.compile(u'([^%s]+)' %(alpha))
        self.rom_dev = re.compile(ur'([a-zA-Z])([%s])' %(alpha))
        self.dev_rom = re.compile(ur'([%s])([a-zA-Z])' %(alpha))
        if self.tokenize:
	    self.alp_noalp = re.compile(ur'([%s\u2010-])([^%s\u2010-])' %(alpha, alpha))
	    self.noalp_alp = re.compile(ur'([^%s\u2010-])([%s\u2010-])' %(alpha, alpha))
	    self.splitsen_r1 = re.compile(u' ([!.?\u06d4]) ([%s])' %(sos_chars))
            self.splitsen_r2 = re.compile(u' ([!.?\u06d4]) ([\)\}\]\'"\u2018\u201c> ]+) ')

	#initialize character maps    
	self.ascii_letters = set(string.ascii_letters)
        self.letter_set = set([unichr(i) for i in range(int("0x0621", 16), int("0x063b", 16))]) | \
                          set([unichr(i) for i in range(int("0x0641", 16), int("0x064b", 16))]) | \
                          set([unichr(i) for i in range(int("0x0674", 16), int("0x06d4", 16))])

	#initialize punctuation map table
	self.punkt_tbl = dict()
        with open('%s/mapping/punkt.map' %dist_dir) as punkt_fp:
       	    for line in punkt_fp:
		line = line.decode('utf-8')
		src, trg = line.split()
		self.punkt_tbl[ord(trg)] = src

	#initialize urdu normalization table
	self.canonical_eq = dict()
        with open('%s/mapping/urdu_urdu.map' %dist_dir) as nu_fp:
       	    for line in nu_fp:
		line = line.decode('utf-8')
		src, trg = line.split()
		self.canonical_eq[ord(src)] = trg

    def feature_extraction(self, letters):
	ngram = 4
        out_letters = list()
        dummies = ["_"] * ngram
        context = dummies + letters + dummies
        for i in range(ngram, len(context)-ngram):
            unigrams  = context[i-ngram: i] + [context[i]] + context[i+1: i+(ngram+1)]
	    bigrams   = ["%s|%s" % (p,q) for p,q in zip(unigrams[:-1], unigrams[1:])]
	    trigrams  = ["%s|%s|%s" % (r,s,t) for r,s,t in zip(unigrams[:-2], unigrams[1:], unigrams[2:])]
	    quadgrams = ["%s|%s|%s|%s" % (u,v,w,x) for u,v,w,x in zip(unigrams[:-3], unigrams[1:], unigrams[2:], unigrams[3:])]
	    ngram_context = unigrams + bigrams + trigrams + quadgrams
            out_letters.append(ngram_context)

        return out_letters

    def predict(self, word):   
        X = self.vectorizer_.transform(word)
        scores = X.dot(self.coef_.T).toarray() 
        y = beamsearch.decode(scores, self.intercept_trans_,
			      self.intercept_init_, self.intercept_final_, 
			      self.beam_width)

	top_seq = list()
	for path in y[:self.topn]:
	    w =  [self.classes_[pid] for pid in path]
            w = ''.join(w).replace('_', '')
            top_seq.append(w)

        return top_seq

    def case_trans(self, word):
        if word in self.lookup:
            return self.lookup[word]
	word_feats = ' '.join(word)
	word_feats = word_feats.replace(u' \u06be', u'\u06be')
	word_feats = word_feats.encode('utf-8')
	word_feats = word_feats.split()
        word_feats = self.feature_extraction(word_feats)
        #op_word = self.con.convert(self.predict(word_feats))
        op_word = self.predict(word_feats)
        self.lookup[word] = op_word

        return op_word

    def unicode_equivalence(self, line):
        line  = line.translate(self.canonical_eq)
        #hamza and mada normalizations
        line = line.replace(u'\u0627\u0653', u'\u0622')
        line = line.replace(u'\u0648\u0654', u'\u0624')
        line = line.replace(u'\u06cc\u0654', u'\u0626')
        line = line.replace(u'\u06d2\u0654', u'\u06d3')
        line = line.replace(u'\u0627\u0654', u'\u0623')
        line = line.replace(u'\u06c1\u0654', u'\u06c0')
        line = line.replace(u'\u06d5\u0654', u'\u06c0')
	
	return line

    def matra_norm(self, line):
        line = line.replace(u'\u0650', '')
        line = line.replace(u'\u064e', '')
        line = line.replace(u'\u064f', '')
        line = line.replace(u'\u0652', '')
        line = line.replace(u'\u0654', '')
        line = line.replace(u'\u0655', '')
        line = line.replace(u'\u0656', '')
        line = line.replace(u'\u0657', '')
        line = line.replace(u'\u064d', '')
        line = re.sub(u'(.)\u0651', r'\1\1', line)  # tashdeed
        line = re.sub(u'\u06cc\u0670([^\u0621-\u063a\u0641-\u064a\u0674-\u06d3])', ur'\u0627\1', line)  # khada-alif
        line = re.sub(u'\u06d2\u0670([^\u0621-\u063a\u0641-\u064a\u0674-\u06d3])', ur'\u0627\1', line)  # khada-alif
        line = re.sub(u'\u0670\u06cc([^\u0621-\u063a\u0641-\u064a\u0674-\u06d3])', ur'\u0627\1', line)  # khada-alif
        line = re.sub(u'\u0670\u06d2([^\u0621-\u063a\u0641-\u064a\u0674-\u06d3])', ur'\u0627\1', line)  # khada-alif
        line = line.replace(u'\u0627\u0670', u'\u0622') #khada-alif
        line = line.replace(u'\u0622\u0670', u'\u0622') #khada-alif
        line = line.replace(u'\u0670', u'\u0627')   #khada-alif
        line = re.sub(u'\u0627\u064b([^\u0621-\u063a\u0641-\u064a\u0674-\u06d3])', ur'\u0646\1', line)  # double-zabar 
        line = re.sub(u'\u064b\u0627([^\u0621-\u063a\u0641-\u064a\u0674-\u06d3])', ur'\u0646\1', line)  # double-zabar
        line = re.sub(u'\u064b([^\u0621-\u063a\u0641-\u064a\u0674-\u06d3])', ur'\u0646\1', line)  # double-zabar
        line = line.replace(u'\u064b', '')  #double-zabar
	line = line.replace(u'\u0644\u0644\u06c1', u'\u0644\u0644\u0627\u06c1')  #llh > llah
	line = re.sub(u'([^\u06a9\u06af\u0686\u062c\u0679\u0688\u062a\u062f\u067e\u0628\u0691])\u06be', ur'\1\u06c1', line) 

	return line

    def max_likelihood(self, n_sentence):
	#unit length sentences
	if len(n_sentence[0]) == 1: 
	    return n_sentence[0][0]

	#transpose
	n_sentence = zip(*n_sentence)
    
	#beamsearch
        auto_tags = self.so_dec.decode(n_sentence, len(n_sentence), self.topn)
	best_sen = [n_sentence[idx][at] for idx, at in enumerate(auto_tags)]

	return  ' '.join(best_sen)
    
    def transliterate(self, text):
	trans_list = list()
	if not isinstance(text, unicode):
	    text = text.decode('utf-8')
        text = self.rom_dev.sub(r'\1 \2', text)
        text = self.dev_rom.sub(r'\1 \2', text)
	#unicode_equivalence
	text = self.unicode_equivalence(text)
	#matra normalization
	text = self.matra_norm(text)
	#tokenization
        if self.tokenize:
	    text = self.alp_noalp.sub(r'\1 \2', text)
	    text = self.noalp_alp.sub(r'\1 \2', text)
        lines = text.split("\n")
	for line in lines:
	    if not line.strip():
		trans_list.append(line.encode('utf-8'))
		continue
	    #split sentences for faster decoding
            if self.tokenize:
	        line = self.splitsen_r1.sub(r' \1\n\2', line)
	        line = self.splitsen_r2.sub(r' \1 \2\n', line)
	    line = ' '.join(line.split())
	    line = line.replace(' ', self.space)
	    sentences = line.split('\n')
	    sen_list = list()	#list of transliterated sentences 
	    for sentence in sentences:
		topn_list = [str() for i in range(self.topn)]   #list of n-best transliterations
		sentence = self.non_alpha.split(sentence)
            	for word in sentence:
	    	    if not word:
	    	        continue
	    	    elif word[0] not in self.letter_set:
	    	        punkt_trans = word.translate(self.punkt_tbl)
	    	        punkt_trans = punkt_trans.encode('utf-8')
			punkt_trans = self.mask_rom.sub(r'_\1_', punkt_trans)
	    	        for b in range(self.topn):
			    topn_list[b] += punkt_trans
	    	    else:
	    	        n_words = self.case_trans(word)
	    	        for b in range(self.topn):
			    topn_list[b] += n_words[b]
	    	for b in range(self.topn):
	    	    topn_list[b] = topn_list[b].split(self.space)
	    	sen_list.append(self.max_likelihood(topn_list))
	    trans_list.append(' '.join(sen_list))

        return self.con.convert('\n'.join(trans_list))
