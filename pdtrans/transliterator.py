#!/usr/bin/env python

import os
import re
import sys

from dev2parab import DP_Transliterator
from parab2dev import PD_Transliterator

class transliterator():

    def __init__(self, source="hindi", beam_width=3, tokenize=True):
        if source=="hindi":
            dpt = DP_Transliterator()
            self.transform = dpt.transliterate
        else:
            pdt = PD_Transliterator(beam_width, tokenize)
            self.transform = pdt.transliterate

    def convert(self, line):
        return self.transform(line)
